export class Product
{
    public id: number;
    public name: string;
    public description: string;
    public category: string;
    public price: number;
    public photo: string;
}