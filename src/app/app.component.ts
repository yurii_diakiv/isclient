import { Component } from '@angular/core';
import { Product } from './models/product';
import { CategoryService } from './services/category-service/category.service';
import { Category } from './models/category';
import { ProductService } from './services/product-service/product.service';
import { PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { SignUpDialogComponent } from './sign-up-dialog/sign-up-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CategoryService, ProductService]
})
export class AppComponent {
  title = 'ISClient';

  categories: Category[] = [];
  idToGet: number = 0;
  productsByCategoryId: Product[] = [];

  pagLength = 3;
  pagPageSize = 3;
  pagPageSizeOptions = [3, 6, 9];


  constructor
    (
      private categoryService: CategoryService,
      private productService: ProductService,
      public loginDialog: MatDialog,
      public signUpDialog: MatDialog
    ) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(x => this.categories = x);
  }

  categoryButtonOnClick(name: string) {
    this.idToGet = this.categories.find(x => x.name == name).id;
    this.productService.getCountOfProductsByCategoryId(this.idToGet).subscribe(x => { this.pagLength = x });
    this.productService.getProductsByCategoryId(this.idToGet, 0, this.pagPageSize).subscribe(x => this.productsByCategoryId = x);
  }

  updateProducts(event?: PageEvent) {
    this.productService.getProductsByCategoryIdPag(this.idToGet, event).subscribe(x => this.productsByCategoryId = x);
  }

  openLoginDialog() {
    const dialogRef = this.loginDialog.open(LoginDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openSignUpDialog() {
    const dialogRef = this.signUpDialog.open(SignUpDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
