import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product';
import { PageEvent } from '@angular/material/paginator';

@Injectable(
//   {
//   providedIn: 'root'
// }
)
export class ProductService {

  constructor(private http: HttpClient) { }

  getCountOfProductsByCategoryId(id: number): Observable<number>
  {
    return this.http.get<number>("https://localhost:44346/api/products/GetCountOfProductsByCategoryId/"+id);
  }

  getProductsByCategoryIdPag(id: number, event?:PageEvent): Observable<Product[]>
  {
    // return this.http.get<Product[]>("https://localhost:44346/api/products/GetCountOfProductsByCategoryId/"+id);
    var from: number;
    var to: number;
    if(event.pageIndex >= event.previousPageIndex)
    {
      from = event.pageIndex*event.pageSize;
      to = from+event.pageSize;
      if(to > event.length)
      {
        to = event.length;
      }
    }
    else
    {
      from = event.pageIndex*event.pageSize-event.pageSize;
      if(from <0)
      {
        from = 0;
      }
      to = from+event.pageSize;
    }
    return this.http.get<Product[]>("https://localhost:44346/api/products/byCategoryId?catId="+id+"&from="+from+"&to="+to);
  }

  getProductsByCategoryId(id: number, from: number, to: number): Observable<Product[]>
  {
    return this.http.get<Product[]>("https://localhost:44346/api/products/byCategoryId?catId="+id+"&from="+from+"&to="+to);
  }
}
